# pansaea

An open world game written in HTML/Javascript

## About

Pansaea is a game that really pushes that definition, and I'm not sure it's pushing it forward. It might be more apt to call it a fictional map, capturing an instant in the decline of a dying world.

I'm making it as part of Kate Barrett's <a href="https://www.glorioustrainwrecks.com/node/12211">Open World Jam</a>.

## Usage

Open the HTML file in a browser. Click and drag with the mouse to move around the map, and mouse over points of interest to read about them.

## Miles to go

As it stands, the game is not yet complete. The biggest thing that needs to be done is filling in the flavor text and images. After that, there are some final tweaks to be made to the code to pull it out of "debug mode", and it'll be good to go.