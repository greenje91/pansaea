//canvas and context initialization
let canvasMap = document.getElementById("map");
let ctxMap = canvasMap.getContext("2d");
let canvasFlavor = document.getElementById("flavor");
let ctxFlavor = canvasFlavor.getContext("2d");

let selected = false;

//init
function startMap() {
    canvasMap.addEventListener("mousemove", onMouseMove);
    buildWaypoints();
}
//puts waypoints from data array on the map
function buildWaypoints() {
    for (i = 0; i < waypointData.length; i++) {
        let img = document.getElementById(waypointData[i].class);
        ctxMap.drawImage(img, waypointData[i].x, waypointData[i].y);
    }
}

//on mouse move, checks for a waypoint and draws flavor on a match
//this is a bit inefficient with variables, but it works, so i ain't touching it
function onMouseMove(ev) {
    let x, y;
    let newX, newY;
    let mainX, mainY;
    let wp, mainWP;

    x = ev.offsetX || ev.layerX;
    y = ev.offsetY || ev.layerY;

    console.log(x, y);

    for (i = 0; i < waypointData.length; i++) {
        wp = waypointData[i];
        if (x >= wp.x && x <= wp.x + 15 && y >= wp.y && y <= wp.y + 15) {
            mainX = wp.x;
            mainY = wp.y;
            mainWP = waypointData[i];
            selected = true;
        }
    }

    if (selected && x >= mainX && x <= mainX + 15 && y >= mainY && y <= mainY + 15) {
        let flavorText = mainWP.flavorText;
        let flavorImage = mainWP.flavorImage;
        ctxMap.globalCompositeOperation = "source-over";
        printPicture(flavorImage, mainX + 10, mainY - 100);
        printWords(flavorText, mainX + 10, mainY + 40);
    } else {
        selected = false;
        ctxMap.clearRect(0, 0, canvasFlavor.clientWidth, canvasFlavor.clientHeight);
        buildWaypoints();
    }
}

//splits up flavor strings and draws them on the canvas with a matching background
function printWords(text, x, y) {
    let fitWidth = 150;
    let lineHeight = 15;
    let bgHeight = 15;
    let bgWidth = fitWidth + 4;
    let bgOffsetY = 10;
    let bgOffsetX = 2;
    let words = text.split(' ');
    let currentLine = 0;
    let idx = 1;
    while (words.length > 0 && idx <= words.length) {
        let str = words.slice(0, idx).join(' ');
        let w = ctxMap.measureText(str).width;
        if (w > fitWidth) {
            ctxMap.fillStyle = "#FFFFFF";
            ctxMap.fillRect(x - bgOffsetX, y - bgOffsetY + (lineHeight * currentLine), bgWidth, bgHeight);
            ctxMap.fillStyle = "#000000";
            if (idx == 1) {
                idx = 2;
            }
            ctxMap.fillText(words.slice(0, idx - 1).join(' '), x, y + (lineHeight * currentLine));
            currentLine++;
            words = words.splice(idx - 1);
            idx = 1;
        } else { idx++; }
    }
    if (idx > 0) {
        ctxMap.fillStyle = "#FFFFFF";
        ctxMap.fillRect(x - bgOffsetX, y - bgOffsetY + (lineHeight * currentLine), bgWidth, bgHeight);
        ctxMap.fillStyle = "#000000";
        ctxMap.fillText(words.join(' '), x, y + (lineHeight * currentLine));
    }
}

function printPicture(image, x, y) {
    let img = new Image;
    //let img = document.getElementById('pic-placeholder');
    img.onload = function() {
        ctxMap.drawImage(img, x, y, 96, 96);
    }
    img.src = "green-ditto.png";
    //img.src = `${image}`;
}

function offsetCalc() {
    let _x = 0;
    let _y = 0;
    let el = canvasMap
    while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
        _x += el.offsetLeft - el.scrollLeft;
        _y += el.offsetTop - el.scrollTop;
        el = el.offsetParent;
    }
    return {top: _y, left: _x};
}

//i haven't spent much time at all working with canvases, so it's a bit janky
//stack exchange helped cover a lot of the holes in my experience:
//https://stackoverflow.com/questions/6215841/create-links-in-html-canvas
//https://stackoverflow.com/questions/20915484/stacking-multiple-canvases-in-html5

