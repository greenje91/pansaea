//to be honest, i pulled most of this from candle's domino 2 code:
//https://kool.tools/domino2/
//not sure what it all does, but it works!

class EventEmitter {
    constructor() {
        this.listeners = {};
    }

    on(event, listener) {
        if (this.listeners[event] === undefined)
            this.listeners[event] = [];
        this.listeners[event].push(listener);
        return () => this.off(event, listener);
    }

    off(event, listener) {
        const listeners = this.listeners[event] || [];
        const index = listeners.indexOf(listener);
        if (index !== -1)
            this.listeners[event].splice(index, 1);
    }

    emit(event, ...args) {
        const listeners = this.listeners[event] || [];
        [...listeners].forEach((listener) => listener(...args));
    }
};

class PointerDrag {
    constructor(event, { clickMovementLimit = 5 } = {}) {
        this.events = new EventEmitter();
        this.pointerId = event.pointerId;
        this.clickMovementLimit = 5;
        this.totalMovement = 0;

        this.downEvent = event;
        this.lastEvent = event; 

        this.removes = [
            listen(document, "pointerup", (event) => {
                if (event.pointerId !== this.pointerId) return;
                this.lastEvent = event;
                this.removes.forEach((remove) => remove());
                this.events.emit("pointerup", event);
                if (this.totalMovement <= clickMovementLimit) {
                    this.events.emit("click", event);
                }
            }),
            listen(document, "pointermove", (event) => {
                if (event.pointerId !== this.pointerId) return;
                this.totalMovement += Math.abs(event.movementX);
                this.totalMovement += Math.abs(event.movementY);
                this.lastEvent = event;
                this.events.emit("pointermove", event);
            }),
        ];
    }

    cancel() {
        this.removes.forEach((remove) => remove());
    }
}

class TransformGesture {
    tryAddPointerDrag(event) {
        const drag = new PointerDrag(event);
        drag.events.on("pointermove", () => {});
    }
}

class PanningScene {
    constructor(container) {
        this.viewport = container.parentElement;
        this.container = container;
        this.transform = new DOMMatrix();

        this.pointerA = undefined;
        this.pointerB = undefined;
        let ratio = 1;

        this.viewport.addEventListener("pointerdown", (event) => {
            killEvent(event);

            if (!this.pointerA) {
                // determine and save the relationship between mouse and scene
                // G = M1^ . S (scene relative to mouse)
                const mouse = this.mouseEventToViewportTransform(event);
                const grab = mouse.invertSelf().multiplySelf(this.transform);
                document.body.style.setProperty("cursor", "grabbing");
                this.viewport.style.setProperty("cursor", "grabbing");
                this.container.classList.toggle("skip-transition", true);

                ratio = 1;
                const drag = new PointerDrag(event);
                drag.events.on("pointermove", (event) => {
                    // preserve the relationship between mouse and scene
                    // D2 = M2 . G (drawing relative to scene)
                    const mouse = this.mouseEventToViewportTransform(event);
                    mouse.scaleSelf(ratio, ratio);
                    this.transform = mouse.multiply(grab);
                    this.refresh();
                });
                drag.events.on("pointerup", (event) => {
                    document.body.style.removeProperty("cursor");
                    this.viewport.style.removeProperty("cursor");
                    this.container.classList.toggle("skip-transition", false);

                    if (this.pointerB) this.pointerB.cancel();

                    this.pointerA = undefined;
                    this.pointerB = undefined;
                });

                this.pointerA = drag;
            } else if (!this.pointerB) {
                const mouseB = this.mouseEventToViewportTransform(event);
                const mouseA = this.mouseEventToViewportTransform(this.pointerA.lastEvent);
                const dx = mouseB.e - mouseA.e;
                const dy = mouseB.f - mouseA.f;
                const initialD = Math.sqrt(dx*dx + dy*dy); 

                this.pointerB = new PointerDrag(event);
                this.pointerB.events.on("pointermove", (event) => {
                    const mouseB = this.mouseEventToViewportTransform(event);
                    const mouseA = this.mouseEventToViewportTransform(this.pointerA.lastEvent);
                    const dx = mouseB.e - mouseA.e;
                    const dy = mouseB.f - mouseA.f;
                    const currentD = Math.sqrt(dx*dx + dy*dy);
                    ratio = currentD / initialD;
                });
                this.pointerB.events.on("pointerup", () => {
                    this.pointerB = undefined;
                });
            }
        });
        
        this.refresh();
    }

    refresh() {
        setElementTransform(this.container, this.transform);
    }

    mouseEventToViewportTransform(event) {
        const rect = this.viewport.getBoundingClientRect();
        const [sx, sy] = [event.clientX - rect.x, event.clientY - rect.y];
        const matrix = (new DOMMatrixReadOnly()).translate(sx, sy);
        return matrix;
    }
}

function killEvent(event) {
    event.stopPropagation();
    event.preventDefault();
}

function start() {
    let scene;
    scene = new PanningScene(document.getElementById("scene"));
    document.querySelector("body").style.transform = "matrix(1, 0, 0, 1, -600, -2112)";
    startMap();
}

function setElementTransform(element, transform) {
    element.style.setProperty("transform", transform.toString());
}

function listen(element, type, listener) {
    element.addEventListener(type, listener);
    return () => element.removeEventListener(type, listener);
}
